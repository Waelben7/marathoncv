'use strict';

var http = require('http');
var express = require('express');
var puppeteer = require('puppeteer');

var app = express();
var server = http.createServer(app);
app.use('/', express.static('public'));

app.post('/generatePDF', async function (req, res) {

  return res.json({ mrigel: true });
});

server.listen(3010, function () {
  return console.log('Listening on port 3010 ');
});