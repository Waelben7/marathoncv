var app = angular.module("marathonCV", []);

app.controller('homeController', function ($scope, $http) {

  $scope.resume = {
    profile: {
      fullName: "Xref",
      address: "Al Jamiyin, Dammam 32254",
      phone: "05********",
      email: "Khalidalamri@marathonksa.com",
      currentPosition: "Backend developer",
      description: `An experienced back-end developer and devops
      with a strong interest in projects that require both
      conceptual and analytical thinking. Fully-committed
      to ensure servers and applications security. Familiar
      with agile methodology and devops tasks. Always
      eager to learn more tricks from anyone`
    },
    education: [
      {
        degree: "Engineering",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        fromYear: 2014,
        toYear: 2019,
        university: "Higher Institute of Applied Science and Technology of Sousse"
      }],
    experiences: [
      {
        company: "Takiacademy",
        description: "Developing an e-learning platform.\
        Using Node js and JavaScript frameworks such\
        Express js, Angular js and MySQL for database.\
        Using modern web approach Webpack, Test driven\
        development , Unit tests, End-to-end tests.",
        fromDate: "01/03/2016",
        toDate: "01/03/2017",
        position: "Fullstack developer",
        address: "Sousse, rue victor hugo",
        currentPosition: false
      }],
    skills: [
      {
        name: "Javascript",
        level: 4
      },
      {
        name: "MongoDB",
        level: 3
      },
      {
        name: "UI/UX",
        level: 3
      }],
    languages: [
      {
        name: "English",
        level: 4
      },
      {
        name: "Frensh",
        level: 4
      },
      {
        name: "Arabic",
        level: 2
      }]
  }

  $scope.getNumber = function (num) {
    return new Array(num);
  }

  $scope.custumDateFormat = function (dateString) {
    if (!dateString)
      return 'Present'

    var tab = dateString.split('/')
    if (tab.length == 3) {
      tab.splice(1, 1)
      dateString = tab.join('/')
    }
    return dateString
  }

  $scope.years = [
    2019,
    2018,
    2017,
    2016,
    2015,
    2014,
    2013,
    2012,
    2011,
    2010,
    2009,
    2008,
    2007,
    2006,
    2005,
    2004,
    2003,
    2002,
    2001,
    2000,
    1999,
    1998,
    1997,
    1996,
    1995
  ]

  var cssData

  function getCssContent() {
    cssData = `
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>    
    <style>`

    $.ajax({
      url: "/en3/assets/css/argon.css",
      dataType: "text",
      success: function (cssResponse) {
        cssData += ("\n" + cssResponse + "\n")
        $.ajax({
          url: "/en3/assets/css/preview.css",
          dataType: "text",
          success: function (cssResponse) {
            cssData += ("\n" + cssResponse + "\n </style>")
          }
        })
      }
    })
  }

  getCssContent()

  $scope.downlaodPDF = function () {
    var str = $('#resumePreview').get(0).innerHTML

    $http({
      method: 'POST',
      url: '/generatePDF',
      data: JSON.stringify({ html: cssData + str }),
      responseType: 'arraybuffer'

    }).then(function (response) {

      const blob = new Blob([response.data])
      const link = document.createElement('a')
      link.download = `MarathonEnCV.pdf`
      link.href = window.URL.createObjectURL(blob)
      link.click()
      delete link;

    }).catch(function (err) {
      console.log(err)
    })

  }

  // ------------------------ Profile

  $scope.profilePanel = angular.copy($scope.resume.profile)

  $scope.saveProfilePanel = function () {
    $scope.resume.profile = angular.copy($scope.profilePanel)
  }

  $scope.cancelProfilePanel = function () {
    $scope.profilePanel = angular.copy($scope.resume.profile)
  }

  // ------------------------ Education

  $scope.showEducationPanel = $scope.resume.education.length == 0 ? true : false
  $scope.educationPanelEditIndex = -1

  $scope.editEducation = function (index) {
    $scope.educationPanelEditIndex = index
    $scope.educationPanel = angular.copy($scope.resume.education[index])
    $scope.showEducationPanel = true
  }

  $scope.deleteEducation = function (index) {
    $scope.resume.education.splice(index, 1)
  }

  $scope.saveEducationPanel = function () {

    if ($scope.educationPanelEditIndex > -1)
      $scope.resume.education[$scope.educationPanelEditIndex] = $scope.educationPanel
    else
      $scope.resume.education.push($scope.educationPanel)

    $scope.educationPanel = null
    $scope.educationPanelEditIndex = -1
    $scope.showEducationPanel = false
  }

  $scope.cancelEducationPanel = function () {
    $scope.educationPanel = {}
    $scope.showEducationPanel = false
  }

  // ------------------------ Experiences

  $scope.showExperiencePanel = $scope.resume.experiences.length == 0 ? true : false
  $scope.ExperiencePanelEditIndex = -1

  $scope.editExperience = function (index) {
    $scope.ExperiencePanelEditIndex = index
    $scope.experiencePanel = angular.copy($scope.resume.experiences[index])
    $scope.showExperiencePanel = true
  }

  $scope.deleteExperience = function (index) {
    $scope.resume.experiences.splice(index, 1)
  }

  $scope.saveExperiencePanel = function () {

    if ($scope.ExperiencePanelEditIndex > -1)
      $scope.resume.experiences[$scope.ExperiencePanelEditIndex] = $scope.experiencePanel
    else
      $scope.resume.experiences.push($scope.experiencePanel)

    $scope.experiencePanel = null
    $scope.ExperiencePanelEditIndex = -1
    $scope.showExperiencePanel = false
  }

  $scope.cancelExperiencePanel = function () {
    $scope.experiencePanel = {}
    $scope.showExperiencePanel = false
  }

  // ------------------------ Skills

  $scope.showSkillPanel = $scope.resume.skills.length == 0 ? true : false
  $scope.SkillPanelEditIndex = -1

  $scope.editSkill = function (index) {
    $scope.SkillPanelEditIndex = index
    $scope.skillPanel = angular.copy($scope.resume.skills[index])
    $scope.showSkillPanel = true
  }

  $scope.deleteSkill = function (index) {
    $scope.resume.skills.splice(index, 1)
  }

  $scope.saveSkillPanel = function () {

    if ($scope.SkillPanelEditIndex > -1)
      $scope.resume.skills[$scope.SkillPanelEditIndex] = $scope.skillPanel
    else
      $scope.resume.skills.push($scope.skillPanel)

    $scope.skillPanel = null
    $scope.SkillPanelEditIndex = -1
    $scope.showSkillPanel = false
  }

  $scope.cancelSkillPanel = function () {
    $scope.skillPanel = {}
    $scope.showSkillPanel = false
  }

  // ------------------------ Languages

  $scope.showLanguagePanel = $scope.resume.languages.length == 0 ? true : false
  $scope.LanguagePanelEditIndex = -1

  $scope.editLanguage = function (index) {
    $scope.LanguagePanelEditIndex = index
    $scope.languagePanel = angular.copy($scope.resume.languages[index])
    $scope.showLanguagePanel = true
  }

  $scope.deleteLanguage = function (index) {
    $scope.resume.languages.splice(index, 1)
  }

  $scope.saveLanguagePanel = function () {

    if ($scope.LanguagePanelEditIndex > -1)
      $scope.resume.languages[$scope.LanguagePanelEditIndex] = $scope.languagePanel
    else
      $scope.resume.languages.push($scope.languagePanel)

    $scope.languagePanel = null
    $scope.LanguagePanelEditIndex = -1
    $scope.showLanguagePanel = false
  }

  $scope.cancelLanguagePanel = function () {
    $scope.languagePanel = {}
    $scope.showLanguagePanel = false
  }

})
