var app = angular.module("marathonCV", []);

app.controller('homeController', function ($scope, $http) {

  $scope.resume = {
    profile: {
      fullName: "وائل بنالطالب",
      address: "سوسة، تروكادرو 4000",
      phone: "246462979",
      email: "waelben7@gmail.com",
      currentPosition: "مهندس برمجيات",
      description: "مبرمج ويب ذو خبرة، مختص في جافاسكريبت، مهتم بالمشاريع التي تتطلب تخطيط و تحليل. "
    },
    education: [
      {
        degree: "هندسة",
        description: "لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه. بروشور او فلاير على سبيل المثال او نماذج مواقع انترنت.",
        fromYear: 2014,
        toYear: 2019,
        university: "المعهد العالي للعلوم التكنولوجية بسوسة"
      }],
    experiences: [
      {
        company: "تقي أكادمي",
        description: "لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه. بروشور او فلاير على سبيل المثال او نماذج مواقع انترنت.",
        fromDate: "02/01/2017",
        toDate: "01/09/2019",
        position: "مبرمج ويب",
        address: "سوسة، شارع فكتور هيقو",
        currentPosition: true
      }],
    skills: [
      {
        name: "جافاسكريب",
        level: 4
      },
      {
        name: "قاعدة البيانات",
        level: 3
      }],
    languages: [
      {
        name: "العربية",
        level: 5
      },
      {
        name: "الإنجليزية",
        level: 4
      },
      {
        name: "الفرنسية",
        level: 2
      }
    ]
  }

  $scope.getNumber = function (num) {
    return new Array(num);
  }

  $scope.custumDateFormat = function (dateString) {
    if (!dateString)
      return 'الآن'

    var tab = dateString.split('/')
    if (tab.length == 3) {
      tab.splice(1, 1)
      dateString = tab.join('/')
    }
    return dateString
  }

  $scope.years = [
    2019,
    2018,
    2017,
    2016,
    2015,
    2014,
    2013,
    2012,
    2011,
    2010,
    2009,
    2008,
    2007,
    2006,
    2005,
    2004,
    2003,
    2002,
    2001,
    2000,
    1999,
    1998,
    1997,
    1996,
    1995
  ]

  var cssData

  function getCssContent() {
    cssData = `
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
      crossorigin="anonymous">
    <style>
    @font-face {
      font-family: 'Cairo';
      src: url('http://localhost:3010/ar/assets/fonts/Cairo-Regular.ttf') format('truetype');
    }
    `

    $.ajax({
      url: "/ar/assets/css/preview.css",
      dataType: "text",
      success: function (cssResponse) {
        cssData += ("\n" + cssResponse + "\n </style>")
      }
    });
  }

  getCssContent()

  $scope.downlaodPDF = function () {
    var str = $('#resumePreview').get(0).innerHTML

    // var myWindow = window.open('', '', 'width=794');
    // myWindow.document.write(cssData + str);
    // myWindow.document.close();
    // myWindow.focus();
    // myWindow.print();
    // setTimeout(() => myWindow.close(), 5000)

    $http({
      method: 'POST',
      url: '/generatePDF',
      data: JSON.stringify({ html: cssData + str }),
      responseType: 'arraybuffer'

    }).then(function (response) {

      const blob = new Blob([response.data])
      const link = document.createElement('a')
      link.download = `MarathonArCV.pdf`
      link.href = window.URL.createObjectURL(blob)
      link.click()
      delete link;

    }).catch(function (err) {
      console.log(err)
    })
  }

  // ------------------------ Profile

  $scope.profilePanel = angular.copy($scope.resume.profile)

  $scope.saveProfilePanel = function () {
    $scope.resume.profile = angular.copy($scope.profilePanel)
  }

  $scope.cancelProfilePanel = function () {
    $scope.profilePanel = angular.copy($scope.resume.profile)
  }

  // ------------------------ Education

  $scope.showEducationPanel = $scope.resume.education.length == 0 ? true : false
  $scope.educationPanelEditIndex = -1

  $scope.editEducation = function (index) {
    $scope.educationPanelEditIndex = index
    $scope.educationPanel = angular.copy($scope.resume.education[index])
    $scope.showEducationPanel = true
  }

  $scope.deleteEducation = function (index) {
    $scope.resume.education.splice(index, 1)
  }

  $scope.saveEducationPanel = function () {

    if ($scope.educationPanelEditIndex > -1)
      $scope.resume.education[$scope.educationPanelEditIndex] = $scope.educationPanel
    else
      $scope.resume.education.push($scope.educationPanel)

    $scope.educationPanel = null
    $scope.educationPanelEditIndex = -1
    $scope.showEducationPanel = false
  }

  $scope.cancelEducationPanel = function () {
    $scope.educationPanel = {}
    $scope.showEducationPanel = false
  }

  // ------------------------ Experiences

  $scope.showExperiencePanel = $scope.resume.experiences.length == 0 ? true : false
  $scope.ExperiencePanelEditIndex = -1

  $scope.editExperience = function (index) {
    $scope.ExperiencePanelEditIndex = index
    $scope.experiencePanel = angular.copy($scope.resume.experiences[index])
    $scope.showExperiencePanel = true
  }

  $scope.deleteExperience = function (index) {
    $scope.resume.experiences.splice(index, 1)
  }

  $scope.saveExperiencePanel = function () {

    if ($scope.ExperiencePanelEditIndex > -1)
      $scope.resume.experiences[$scope.ExperiencePanelEditIndex] = $scope.experiencePanel
    else
      $scope.resume.experiences.push($scope.experiencePanel)

    $scope.experiencePanel = null
    $scope.ExperiencePanelEditIndex = -1
    $scope.showExperiencePanel = false
  }

  $scope.cancelExperiencePanel = function () {
    $scope.experiencePanel = {}
    $scope.showExperiencePanel = false
  }

  // ------------------------ Skills

  $scope.showSkillPanel = $scope.resume.skills.length == 0 ? true : false
  $scope.SkillPanelEditIndex = -1

  $scope.editSkill = function (index) {
    $scope.SkillPanelEditIndex = index
    $scope.skillPanel = angular.copy($scope.resume.skills[index])
    $scope.showSkillPanel = true
  }

  $scope.deleteSkill = function (index) {
    $scope.resume.skills.splice(index, 1)
  }

  $scope.saveSkillPanel = function () {

    if ($scope.SkillPanelEditIndex > -1)
      $scope.resume.skills[$scope.SkillPanelEditIndex] = $scope.skillPanel
    else
      $scope.resume.skills.push($scope.skillPanel)

    $scope.skillPanel = null
    $scope.SkillPanelEditIndex = -1
    $scope.showSkillPanel = false
  }

  $scope.cancelSkillPanel = function () {
    $scope.skillPanel = {}
    $scope.showSkillPanel = false
  }

  // ------------------------ Languages

  $scope.showLanguagePanel = $scope.resume.languages.length == 0 ? true : false
  $scope.LanguagePanelEditIndex = -1

  $scope.editLanguage = function (index) {
    $scope.LanguagePanelEditIndex = index
    $scope.languagePanel = angular.copy($scope.resume.languages[index])
    $scope.showLanguagePanel = true
  }

  $scope.deleteLanguage = function (index) {
    $scope.resume.languages.splice(index, 1)
  }

  $scope.saveLanguagePanel = function () {

    if ($scope.LanguagePanelEditIndex > -1)
      $scope.resume.languages[$scope.LanguagePanelEditIndex] = $scope.languagePanel
    else
      $scope.resume.languages.push($scope.languagePanel)

    $scope.languagePanel = null
    $scope.LanguagePanelEditIndex = -1
    $scope.showLanguagePanel = false
  }

  $scope.cancelLanguagePanel = function () {
    $scope.languagePanel = {}
    $scope.showLanguagePanel = false
  }

})
