import http from 'http'
import morgan from 'morgan'
import express from 'express'
import puppeteer from 'puppeteer'
import bodyParser from 'body-parser'

const app = express()
const server = http.createServer(app)

app.use(bodyParser.json({ limit: "4mb" }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(morgan('dev'))

app.use('/', express.static('public'))

let browser

async function initPuppeteer() {
  try {
    browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] });
  } catch (error) {
    console.log(error)
  }
}

initPuppeteer()

app.post('/generatePDF', async function (req, res) {
  try {

    const page = await browser.newPage();
    await page.setContent(req.body.html, { waitUntil: 'networkidle0' });
    const pdf = await page.pdf({
      format: 'A4',
      printBackground: true
    });
    await page.close();
    
    res.set({ 'Content-Type': 'application/pdf', 'Content-Length': pdf.length })
    return res.send(pdf)

  } catch (error) {
    return res.json(error)
  }

})

server.listen(3010, () => console.log('Listening on port 3010 '))
